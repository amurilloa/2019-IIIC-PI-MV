/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introductorio;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class EntradaSalida {

    public static void main(String[] args) {
        
        String nombre = JOptionPane.showInputDialog(null, "Nombre", "Titulo", JOptionPane.QUESTION_MESSAGE);
        int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Edad", "Titulo", JOptionPane.QUESTION_MESSAGE));
        
        String msj = "Nombre: " + nombre + "\nEdad: " + edad;
        JOptionPane.showMessageDialog(null, msj, "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, msj, "ERROR", JOptionPane.ERROR_MESSAGE);
        JOptionPane.showMessageDialog(null, msj, "ADVERTENCIA", JOptionPane.WARNING_MESSAGE);
        JOptionPane.showMessageDialog(null, msj, "SIN ICONO", JOptionPane.PLAIN_MESSAGE);
        
        
        int res  = JOptionPane.showConfirmDialog(null, "¿Esta seguro que desea salir?", "Salir...", JOptionPane.YES_NO_OPTION);
        System.out.println(res);
        if(res == JOptionPane.YES_OPTION){
            System.out.println("Gracias por aplicación");
        } 
        
        //Titulo: Entrevista - UTN v0.1
        //Datos: Nombre, Apellidos, Teléfono, Genero
        //Trabaja: SI/NO
        //      - Lugar de Trabajo: 
        //      - Salario Mensual
        //Mostrar un resumes: 
        
        /*-------------------------------
            Nombre: Allan Murillo Alfaro
            Género: Masculino 
            Teléfono: 8526-2638
            Lugar: UTN
            Salario: 500000
         --------------------------------
        */
        
    }
}
