package introductorio;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Introductorio {
    
    public static void main(String[] args) {
        System.out.println("A\tB\tC\n"
                + "1\t2\t3\n"
                + "1\t2\t3\n"
                + "1\t2\t3");

        System.out.println("\"Hola\"");
        System.out.println("El caracter de escape \"\\n\" cambia de linea");
        System.out.println("El unicode \"\\u0040\" vale \u0040");
        System.out.println("El unicode \"\\u00D1\" vale \u00D1");

        int y;  //declaración
        y = 10; //Inicialización 
        y = 12; //Asignación 

        int x = 10; //Declaración e inicialización
        x = 23;
        final int PRECIO_BASE;
        PRECIO_BASE = 10;
        Double precio;
        String texto;

        boolean isReal = true;
        byte b = 127;
        short s = -29000;
        int i = 100000;
        long l = 999999999999L;
        float f1 = 234.99F;
        double d;
        char cvalue = '4';
        System.out.println(cvalue);
        
        final double PI = 3.1459126; //Constante
        System.out.println((byte)120);
        System.out.println((int)32.23);
        System.out.println((double)120);
        System.out.println((char)64);

        double x2 = 2;
        System.out.println(7/x2);
        int a = 12;
        a = Integer.parseInt("5");
        double b1 = Double.parseDouble("12.3");
        boolean b2 = Boolean.parseBoolean("True");
        
//        System.out.println("a: " + a);
//        System.out.println("a++: " + a++);
//        System.out.println("a: " + a);
//        System.out.println("++a: " + ++a);
//        System.err.println("ERROR: Hola mundo!!");
//
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Digite su nombre: ");
//        String nom = sc.nextLine();
//        
//        System.out.print("Digite su apellido: ");
//        String ape = sc.nextLine();
//        System.out.println("Su nombre es " + nom + " " + ape);
//        
//        System.out.print("Edad: ");
//        int edad = sc.nextInt();
//        System.out.println("Su edad es: " + edad);
//        
//        System.out.print("Año: ");
//        int anno = sc.nextInt();
//        System.out.println("Su edad es: " + anno);

        String nombre = JOptionPane.showInputDialog(null, "Nombre", "Titulo", JOptionPane.QUESTION_MESSAGE);
        int edad = Integer.parseInt(JOptionPane.showInputDialog(null, "Edad", "Titulo", JOptionPane.QUESTION_MESSAGE));
        JOptionPane.showMessageDialog(null, "Nombre: " + nombre + "\nEdad: " + edad, "Titulo", JOptionPane.PLAIN_MESSAGE);
    }

}
