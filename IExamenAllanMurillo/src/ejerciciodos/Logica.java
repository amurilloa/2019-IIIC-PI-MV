/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciodos;

import ejerciciouno.Articulo;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    public int[] generar(int tam) {
        int[] arreglo = new int[tam];
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = (int) (Math.random() * 10) + 1;
        }
        return arreglo;
    }

    public String listar(int[] arr) {
        String texto = "";
        for (int num : arr) {
            texto += num + " ";
        }
        return texto;
    }

    public String duplicados(int[] arr) {
        int mayor = 0;
        int cant = 0;
        int[] temp = new int[arr.length];
//        {1,2,3,4,8,5,1,4,8,1}
        for (int num : arr) { //1
            int rep = 0;
            for (int valor : arr) {
                if (num == valor) {
                    rep++;
                }
            }
            if (rep > cant) {
                mayor = num;
                cant = rep;
            }
        }
        return String.format("Número %d Cantidad %d", mayor, cant);
    }

    /**
     * 
     * @param arr
     * @param num
     * @return 
     */
    public boolean eliminar(int[] arr, int num) {
        boolean elimino = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                arr[i] = 0;
                elimino = true;
            }
        }
        return elimino;
    }

}
