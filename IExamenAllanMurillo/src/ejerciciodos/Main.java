/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciodos;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        int[] arr = log.generar(10);

        String menu = "1. Generar Arreglo\n"
                + "2. Duplicados\n"
                + "3. Eliminar\n"
                + "4. Salir";

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    int num = Util.leerInt("Cantidad");
                    arr = log.generar(num);
                    Util.mostrar(log.listar(arr));
                    break;
                case 2:
                    Util.mostrar(log.listar(arr) + "\n" + log.duplicados(arr));
                    break;
                case 3:
                    num = Util.leerInt("Número a Eliminar");
                    Util.mostrar(log.listar(arr) + "\n" + log.eliminar(arr, num)
                            + "\n" + log.listar(arr));
                    break;
                case 4:
                    break APP;
            }

        } while (true);
    }

}
