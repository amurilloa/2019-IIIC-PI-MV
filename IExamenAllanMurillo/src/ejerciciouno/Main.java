/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {
    
    public static void main(String[] args) {
        Logica log = new Logica();
        
        log.registrarArticulo(new Articulo(1548, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1549, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1551, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1552, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1553, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1554, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1555, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1556, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1557, "Allan Murillo ALfaro", 15,526.36));
        log.registrarArticulo(new Articulo(1559, "Allan Murillo ALfaro", 15,526.36));
        
        
        String menu = "1. Registrar Articulo\n"
                + "2. Buscar\n"
                + "3. Articulo Favorito\n"
                + "4. Listar\n"
                + "5. Salir";
        
        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    Articulo art = new Articulo();
                    art.setNumero(Util.leerInt("Número:"));
                    art.setNombre(Util.leerTexto("Nombre:"));
                    art.setPrecio(Util.leerNum("Precio:"));
                    art.setCantidad(Util.leerInt("Cantidad: "));
                    log.registrarArticulo(art);
                    break;
                case 2:
                    Util.mostrar(log.buscar());
                    break;
                case 3:
                    Util.mostrar(log.favorito());
                    break;
                case 4:
                    Util.mostrar(log.listar());
                    break;
                case 5:
                    
                    break APP;
            }
            
        } while (true);
    }
}
