/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private Articulo[] articulos;

    public Logica() {
        articulos = new Articulo[20];
    }

    public void registrarArticulo(Articulo a) {
        for (int i = 0; i < articulos.length; i++) {
            if (articulos[i] == null) {
                articulos[i] = a;
                break;
            }
        }
    }

    public String buscar() {
        String texto = "";
        for (Articulo art : articulos) {
            if (art != null && art.getCantidad() * art.getPrecio() > 200) {
                texto += String.format("%6d | %10.2f | %5d | %s\n", art.getNumero(), art.getPrecio(),
                        art.getCantidad(), art.getNombre());
            }
        }
        return texto;
    }

    public String listar() {
        String texto = "";
        for (Articulo art : articulos) {
            if (art != null) {
                texto += String.format("%6d | %10.2f | %5d | %s\n", art.getNumero(), art.getPrecio(),
                        art.getCantidad(), art.getNombre());
            }
        }
        return texto;
    }
    
    
    public String favorito() {
        double monto = 0;
        String texto = "";
        for (Articulo art : articulos) {
            if (art != null && (art.getCantidad()*art.getPrecio() > monto)) {
                monto = art.getCantidad()*art.getPrecio();
                texto = String.format("%6d | %10.2f | %s\n", art.getNumero(), monto, art.getNombre());
            }
        }
        return texto;
    }

}
