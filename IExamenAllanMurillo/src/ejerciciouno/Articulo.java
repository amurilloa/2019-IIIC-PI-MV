/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

/**
 *
 * @author Allan Murillo
 */
public class Articulo {

    private int numero;
    private String nombre;
    private int cantidad;
    private double precio;

    public Articulo() {
    }

    public Articulo(int numero, String nombre, int cantidad, double precio) {
        this.numero = numero;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Articulo{" + "numero=" + numero + ", nombre=" + nombre + ", cantidad=" + cantidad + ", precio=" + precio + '}';
    }

}
