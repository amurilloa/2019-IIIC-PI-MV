/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Logica {
    
    
    // nivel de acceso + tipo de retorno + nombre del método ( tipo de argumento + argumento* + ){ }
    /*
     * nivel de acceso: public cualquier parte de la aplicacion o private solo dentro de la misma clase
     * tipo de retorno: int, String, Perro, Object, void
     */
    
    /**
     * Sumar dos números enteros
     * @param numUno int primer número
     * @param numDos int segundo número
     * @return int resultado de la suma de numUno con numDos
     */
    public int sumar(int numUno, int numDos){
        int res = numUno + numDos;
        return res;
    }
    
    public static int restar(int numUno, int numDos){
        int res = numUno - numDos;
        return res;
    }
     
}
