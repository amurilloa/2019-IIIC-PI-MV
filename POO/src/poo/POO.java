/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class POO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

//        Logica log = new Logica();
//
//        String s = "asd";
//
//        System.out.println("Interactuar con el usuario");
//        int n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
//        int n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
//
//        String res = String.format(">> %d + %d = %d", n1, n2, log.sumar(n1, n2));
//        res += String.format("\n>> %d - %d = %d", n1, n2, Logica.restar(n1, n2));
//
//        JOptionPane.showMessageDialog(null, res);

        Garrobo g1 = new Garrobo();
        g1.setNombre(JOptionPane.showInputDialog("Nombre del Garrobo"));
        System.out.println(g1.getNombre());
        Garrobo g2 = new Garrobo("Garrobo 2");
        System.out.println(g2.getNombre());
        System.out.println(g2.getDistancia());
        Garrobo g3 = new Garrobo("Juancito", 6,2);
        System.out.println(g3);
        System.out.println(g3.calcularDistancia(9));
        System.out.println(g3.calcularTiempo(50));
        System.out.println(g3.getNombre());
        System.out.println(g3.getDistancia());
        System.out.println(g3.getTiempo());
        
        Computadora c=new Computadora(13123, "DELL", "G5", "i7", "8gb", 625, false);
        System.out.println(c);
        Monitor m = new Monitor("12330", "DELL", "PATITO", "17\"", 120000, false);
        System.out.println(m);
    }
}


//Dos clases cualquiera, con minimo 6 atributos de 3 tipos diferentes.
//Atributos, contructores(min vacio y lleno), get, set, toString
//Dos instancias uno de cada uno (llenas) e imprimir el estado de cada una. 

