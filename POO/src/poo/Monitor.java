/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Monitor {

    private String numeroSerie;
    private String marca;
    private String modelo;
    private String dimension;
    private int precio;
    private boolean tactil;

    public Monitor() {
    }

    public Monitor(String numeroSerie, String marca, String modelo, String dimension, int precio, boolean tactil) {
        this.numeroSerie = numeroSerie;
        this.marca = marca;
        this.modelo = modelo;
        this.dimension = dimension;
        this.precio = precio;
        this.tactil = tactil;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isTactil() {
        return tactil;
    }

    public void setTactil(boolean tactil) {
        this.tactil = tactil;
    }

    @Override
    public String toString() {
        return "Monitor{" + "numeroSerie=" + numeroSerie + ", marca=" + marca + ", modelo=" + modelo + ", dimension=" + dimension + ", precio=" + precio + ", tactil=" + tactil + '}';
    }
    
}
