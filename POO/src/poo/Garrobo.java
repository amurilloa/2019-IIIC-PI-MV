/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Garrobo { //Primera letra en mayúscula y singular 
    //Atributos nivel acceso(private) + tipo + nombre;

    private String nombre;
    private double distancia;
    private double tiempo;

    //Constructor: Forma en la que podemos inicializar las instancias
    public Garrobo() {
    }

    public Garrobo(String nombre) {
        this.nombre = nombre;
    }

    public Garrobo(String nombre, double distancia, double tiempo) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

    //Métodos *Opcionales*
    private double velocidad() {
        return distancia / tiempo;
    }

    public double calcularTiempo(double distancia) {
        return distancia / velocidad();
    }

    public double calcularDistancia(double tiempo) {
        return tiempo * velocidad();
    }

    //Getters Setters
    //GET: nivel acceso(public) + tipo atributo + getAtributo(){return atributo}
    //SET: nivel acceso(public) + void + setAtributo(tipo atribito parametro){atributo = parametro}
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    //toString
    @Override
    public String toString() {
        double vel = Math.round(velocidad() * 100) / 100.0;
        return "Garrobo ==> " + "Nombre: " + nombre + " | Distancia: " + distancia + "| Tiempo: " + tiempo + "| Velocidad: " + vel + "m/s";
    }
}
