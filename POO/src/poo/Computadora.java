/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Computadora {
    private int numeroSerie;
    private String marca;
    private String modelo;
    private String procesador;
    private String ram;
    private int precio;
    private boolean tactil;

    public Computadora() {
    }

    public Computadora(int numeroSerie, String marca, String modelo, String procesador, String ram, int precio, boolean tactil) {
        this.numeroSerie = numeroSerie;
        this.marca = marca;
        this.modelo = modelo;
        this.procesador = procesador;
        this.ram = ram;
        this.precio = precio;
        this.tactil = tactil;
    }

    public int getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(int numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isTactil() {
        return tactil;
    }

    public void setTactil(boolean tactil) {
        this.tactil = tactil;
    }

    @Override
    public String toString() {
        return "Computadora{" + "numeroSerie=" + numeroSerie + ", marca=" + marca + ", modelo=" + modelo + ", procesador=" + procesador + ", ram=" + ram + ", precio=" + precio + ", tactil=" + tactil + '}';
    }
    
    
}
