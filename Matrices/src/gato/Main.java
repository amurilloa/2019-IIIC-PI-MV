/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();

        log.setJ1(Util.leerTexto("Jugador 1"));
        log.setJ2(Util.leerTexto("Jugador 2"));
        APP:
        do {
            int fila = Util.leerInt(log.getJugadorActual() + "\n"
                    + log.imprimir() + "\nFila: ");
            int columna = Util.leerInt(log.getJugadorActual() + "\n"
                    + log.imprimir() + "\nFila: " + fila + "\nColumna: ");
            log.marcar(fila, columna);

            if (log.gano()) {
                Util.mostrar(log.imprimir() + "\nFelicidades el ganador es: " + log.getGanador());
                break;
            } else if (log.empate()) {
                Util.mostrar(log.imprimir() + "\n:El juego termina en empate");
            }
        } while (true);
    }
}
