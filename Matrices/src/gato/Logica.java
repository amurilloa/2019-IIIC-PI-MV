/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private String j1;
    private String j2;
    private boolean turno;
    private final char[][] tablero;

    private final char MARCA_J1 = 'X';
    private final char MARCA_J2 = 'O';

    public Logica() {
        tablero = new char[3][3];
        limpiar();
        rifar();
    }

    private void rifar() {
        turno = (int) (Math.random() * 10) + 1 < 5;
    }

    private void limpiar() {
        for (int fil = 0; fil < tablero.length; fil++) {
            for (int col = 0; col < tablero[fil].length; col++) {
                tablero[fil][col] = '_';
            }
        }
    }

    public String imprimir() {
        String txt = "   1  2  3\n";
        int nf = 1;
        for (char[] f : tablero) {
            txt += nf++ + "  ";
            for (char c : f) {
                txt += c + "  ";
            }
            txt += "\n";
        }
        return txt;
    }

    public void marcar(int fila, int columna) {
        if (fila > 0 && fila < 4 && columna > 0 && columna < 4) {
            if (tablero[fila - 1][columna - 1] == '_') {
                tablero[fila - 1][columna - 1] = turno ? MARCA_J1 : MARCA_J2;
                turno = !turno;
            }
        }
    }

    public boolean gano() {
        //Filas
        for (int f = 0; f < 3; f++) {
            if (tablero[f][0] != '_'
                    && tablero[f][0] == tablero[f][1]
                    && tablero[f][0] == tablero[f][2]) {
                return true;
            }
        }
        //Columnas
        for (int c = 0; c < 3; c++) {
            if (tablero[0][c] != '_'
                    && tablero[0][c] == tablero[1][c]
                    && tablero[0][c] == tablero[2][c]) {
                return true;
            }
        }

        if (tablero[0][0] != '_'
                && tablero[0][0] == tablero[1][1]
                && tablero[0][0] == tablero[2][2]) {
            return true;
        }

        if (tablero[0][2] != '_'
                && tablero[0][2] == tablero[1][1]
                && tablero[0][2] == tablero[2][0]) {
            return true;
        }

        return false;
    }

    public String getJugadorActual() {
        return turno ? j1 + "(" + MARCA_J1 + ")" : j2 + "(" + MARCA_J2 + ")";
    }

    public String getGanador() {
        return turno ? j2 + "(" + MARCA_J2 + ")" : j1 + "(" + MARCA_J1 + ")";
    }

    public void setJ1(String j1) {
        this.j1 = j1;
    }

    public void setJ2(String j2) {
        this.j2 = j2;
    }

    public boolean empate() {
        for (char[] fil : tablero) {
            for (char c : fil) {
                if (c == '_') {
                    return false;
                }
            }
        }
        return true;
    }

}
