/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Matriz m = new Matriz();
        String menu = "1. Convertir a pares\n"
                + "2. Convertir a impares\n"
                + "3. Promedio\n"
                + "4. Promedio por fila\n"
                + "5. Promedio por columnas\n"
                + "6. Salir";

        m.llenar();
        Util.mostrar(m.imprimir());
        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    String res = "Antes-->\n" + m.imprimir();
                    m.convertirPares();
                    res += "Después-->\n" + m.imprimir();
                    Util.mostrar(res);
                    break;
                case 2:
                    res = "Antes-->\n" + m.imprimir();
                    m.convertirImpares();
                    res += "Después-->\n" + m.imprimir();
                    Util.mostrar(res);
                    break;
                case 3:
                    Util.mostrar(m.imprimir() + "\nPromedio: " + String.format("%.2f",m.promedio()));
                    break;
                case 4:
                    Util.mostrar(m.promedioFila());
                    break;
                case 5:
                    Util.mostrar(m.promedioColumna());
                    break;
                case 6:
                    break APP;
            }
        } while (true);

//        int numero = 10;
//        int[] arreglo = {2, 3, 4, 5};//new int[4];
//        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};//new int[2][3]; // 
//        
//        for (int i = 0; i < arreglo.length; i++) {
//            //arreglo[i] = Util.leerInt("#" + (i + 1));
//            System.out.print(arreglo[i] + " ");
//
//        }
//
//        System.out.println("");
//        for (int f = 0; f < matriz.length; f++) {
//            for (int c = 0; c < matriz[f].length; c++) {
//                matriz[f][c] = 12;
//                System.out.print(matriz[f][c] + " ");
//            }
//            System.out.println("");
//        }
//
//        for (int num : arreglo) {
//            System.out.print(num + " ");
//        }
//
//        System.out.println("");
//
//        for (int[] fila : matriz) {
//            for (int columnas : fila) {
//                System.out.print(columnas + " ");
//            }
//            System.out.println("");
//        }
//
//        System.out.println("");
    }

}
