/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author Allan Murillo
 */
public class Matriz {
    
    private final int[][] matriz;

    public Matriz() {
        matriz = new int[3][4];
    }

    public Matriz(int filas, int columnas) {
        this.matriz = new int[filas][columnas];
    }

    public String imprimir() {
        String txt = "";
        for (int[] fila : matriz) {
            for (int col : fila) {
                txt += col + " ";
            }
            txt += "\n";
        }
        return txt;
    }

    public void llenar() {
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                matriz[f][c] = (int) (Math.random() * 8) + 1;
            }
        }
    }

    public void convertirPares() {
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                if (matriz[f][c] % 2 != 0) {
                    matriz[f][c]++;
                }
            }
        }
    }

    public void convertirImpares() {
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                if (matriz[f][c] % 2 == 0) {
                    matriz[f][c]++;
                }
            }
        }
    }

    public double promedio() {
        double suma = 0;
        int cant = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                suma += matriz[f][c];
                cant++;
            }
        }
        double prom = suma / cant;
        return prom;
    }

    public String promedioFila() {
        String res = "";
        for (int f = 0; f < matriz.length; f++) {
            double suma = 0;
            int cant = 0;
            for (int c = 0; c < matriz[f].length; c++) {
                suma += matriz[f][c];
                res += matriz[f][c] + " ";
                cant++;
            }
            double prom = suma / cant;
            res += " = " + String.format("%.2f",prom) + "\n";
        }
        return res;
    }

    public String promedioColumna() {
        String res = "";
        for (int c = 0; c < matriz[0].length; c++) {
            double suma = 0;
            int cant = 0;
            for (int f = 0; f < matriz.length; f++) {
                suma += matriz[f][c];
                res += matriz[f][c] + " ";
                cant++;
            }
            double prom = suma / cant;
            res += " = " + String.format("%.2f",prom) + "\n";
        }
        return res;
    }

}
