/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import util.Util;

/**
 *
 * @author Allan
 */
public class ManejadorArchivos {

    public static final String BUZON = "buzon";
    public static final String PRODU = "produccion";
    public static final String USERS = "datos/user.txt";
    private final String PAPEL = "papelera";

    //C:\Proyectos\personas.txt
    //personas.txt
    public String leerTextoArchivo(String nombreArchivo) {
        String texto = "";
        FileReader archivo = null;
        String linea = "";
        try {
            archivo = new FileReader(nombreArchivo);
            BufferedReader lector = new BufferedReader(archivo);
            while ((linea = lector.readLine()) != null) {
                texto += linea + "\n";
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Archivo no encontrado");
        } catch (IOException e) {
            throw new RuntimeException("Ocurrio un error de entrada / salida");
        } finally {
            if (archivo != null) {
                try {
                    archivo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return texto;
    }

    public void escribirTextoArchivo(String nombreArchivo, String texto) {
        FileWriter salida = null;
        try {
            salida = new FileWriter(nombreArchivo);
            BufferedWriter escritor = new BufferedWriter(salida);
            salida.write(texto.replaceAll("\n", "\r\n"));
            escritor.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (salida != null) {
                try {
                    salida.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Carta[] cargar(String ruta) {
        Carta[] cartas = new Carta[100];
        int i = 0;
        File f = new File(ruta);
        File[] archivos = f.listFiles();
        for (File archivo : archivos) {
            Carta c = new Carta();
            c.setRuta(archivo.getPath());
            String contenido = leerTextoArchivo(archivo.getPath());
            c.setContenido(contenido);
            c.setFecha(Util.textoFecha(contenido.split("\n")[0]));
            cartas[i++] = c;
        }
        return cartas;
    }

    public void moverCarta(boolean aprobar, String ruta) {
        File f = new File(ruta);
        f.renameTo(new File((aprobar ? PRODU : PAPEL) + "/" + f.getName()));
    }

}
