/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

/**
 *
 * @author Allan Murillo
 */
public class Juguete {

    private int codigo;
    private String nombre;
    private int canMaderaMetal;
    private int canClaTor;
    private int canPintura;

    public Juguete() {
    }

    public Juguete(int codigo, String nombre, int canMaderaMetal, int canClaTor, int canPintura) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.canMaderaMetal = canMaderaMetal;
        this.canClaTor = canClaTor;
        this.canPintura = canPintura;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCanMaderaMetal() {
        return canMaderaMetal;
    }

    public void setCanMaderaMetal(int canMaderaMetal) {
        this.canMaderaMetal = canMaderaMetal;
    }

    public int getCanClaTor() {
        return canClaTor;
    }

    public void setCanClaTor(int canClaTor) {
        this.canClaTor = canClaTor;
    }

    public int getCanPintura() {
        return canPintura;
    }

    public void setCanPintura(int canPintura) {
        this.canPintura = canPintura;
    }

    @Override
    public String toString() {
        return "Juguete{" + "codigo=" + codigo + ", nombre=" + nombre + ", canMaderaMetal=" + canMaderaMetal + ", canClaTor=" + canClaTor + ", canPintura=" + canPintura + '}';
    }

}
