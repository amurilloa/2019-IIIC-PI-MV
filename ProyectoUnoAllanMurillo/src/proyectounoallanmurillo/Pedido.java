/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

import java.util.Date;

/**
 *
 * @author Allan Murillo
 */
public class Pedido {
    
    private int codigo;
    private String cedula;
    private Date fecha;
    private char estado;//P:pedido, T:Terminado, E:Entregado

    public Pedido() {
    }

    public Pedido(int codigo, String cedula, Date fecha, char estado) {
        this.codigo = codigo;
        this.cedula = cedula;
        this.fecha = fecha;
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    
    
    @Override
    public String toString() {
        return "Pedido{" + "codigo=" + codigo + ", cedula=" + cedula + ", fecha=" + fecha + ", estado=" + estado + '}';
    }
    
    
    
}
