/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

import java.util.Date;

/**
 *
 * @author Allan Murillo
 */
public class Nino {
    
    private String cedula;
    private String nombre;
    private String direccion;
    private char genero;
    private Date fechaNacimiento;

    public Nino() {
    }

    public Nino(String cedula, String nombre, String direccion, char genero, Date fechaNacimiento) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Nino{" + "cedula=" + cedula + ", nombre=" + nombre + ", direccion=" + direccion + ", genero=" + genero + ", fechaNacimiento=" + fechaNacimiento + '}';
    }
}
