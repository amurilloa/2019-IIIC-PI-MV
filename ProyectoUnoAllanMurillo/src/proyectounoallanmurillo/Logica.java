/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

import java.time.LocalDate;
import java.util.Date;
import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private int madera;
    private int clavos;
    private int pintura;

    private Carta[] buzon;
    private Carta[] produccion;

    private final Usuario[] usuarios;
    private final Nino[] ninos;
    private final Pedido[] pedidos;
    private final Juguete[] recetas;
    private final ManejadorArchivos ma;

    public Logica() {
        recetas = new Juguete[]{
            new Juguete(101, "Bicicleta", 5, 1, 3),
            new Juguete(102, "Moto", 10, 0, 5),
            new Juguete(103, "Muñeca", 3, 0, 6),
            new Juguete(104, "Casa Muñecas", 10, 5, 8)};

        usuarios = new Usuario[10];
        ninos = new Nino[100];
        produccion = new Carta[100];
        pedidos = new Pedido[100];
        ma = new ManejadorArchivos();
        buzon = ma.cargar(ManejadorArchivos.BUZON);
        produccion = ma.cargar(ManejadorArchivos.PRODU);

        //TODO: Cargar desde un archivo....
        madera = 100;
        clavos = 100;
        pintura = 100;
        cargarUsuarios();
    }

    /**
     * Registra un usuario en el arreglo de usuarios
     *
     * @param u Usuario con los datos digitados por "Usuario"
     * @return String mensaje de éxito o error
     */
    public String registrarUsuario(Usuario u, boolean guardar) {
        for (int i = 0; i < usuarios.length; i++) {
            if (usuarios[i] == null) {
                usuarios[i] = u;
                if (guardar) {
                    guardarUsuarios();
                }
                return "Usuario registrado con éxito";
            }
        }
        return "No es posible registrar más usuarios";
    }

    public void guardarUsuarios() {
        String txt = "";
        for (Usuario usuario : usuarios) {
            if (usuario != null) {
                txt += usuario.getData() + "\n";
            }
        }
        ma.escribirTextoArchivo(ManejadorArchivos.USERS, txt);
    }

    private void cargarUsuarios() {
        String txt = ma.leerTextoArchivo(ManejadorArchivos.USERS);
        String[] usuarios = txt.split("\n");
        for (String linea : usuarios) {
            String[] datos = linea.split(",");
            Usuario u = new Usuario();
            u.setCedula(datos[0]);
            u.setNombre(datos[1]);
            u.setCorreo(datos[2]);
            u.setContrasena(datos[3]);
            u.setTipo(datos[4].charAt(0));
            registrarUsuario(u, false);
        }
    }

    public String registrarNino(Nino n) {
        for (int i = 0; i < ninos.length; i++) {
            if (ninos[i] == null) {
                ninos[i] = n;
                return "Niño/a registrado/a con éxito";
            }
        }
        return "No es posible registrar más niños/as";
    }

    public Usuario iniciarSesion(String ced, String pas) {
        for (Usuario usuario : usuarios) {
            if (usuario != null && ced.equalsIgnoreCase(usuario.getCedula())
                    && pas.equals(usuario.getContrasena())) {
                return usuario;
            }
        }
        return null;
    }

    public String cargarCartas(Date date, Carta[] cartas) {
        String txt = "";
        int i = 1;
        for (Carta carta : cartas) {
            if (carta != null && carta.getFecha().equals(date)) {
                txt += i + ". " + carta.getRuta() + "\n";
            }
            i++;
        }
        return txt;
    }

    public String cargarCartas(Carta[] cartas) {
        String txt = "";
        int i = 1;
        for (Carta carta : cartas) {
            if (carta != null) {
                txt += i++ + ". " + carta.getRuta() + "\n";
            }
        }
        return txt;
    }

    public String cargarCartasHoy(Carta[] cartas) {

        String fecha = hoy();
        return cargarCartas(Util.textoFecha(fecha), cartas);
    }

    public String hoy() {
        LocalDate d = LocalDate.now();
        String fecha = d.getDayOfMonth() + "/" + d.getMonthValue() + "/" + d.getYear();
        return fecha;
    }

    public Carta getCartaBuzon(int pos) {
        return buzon[pos - 1];
    }

    public Carta getCartaProduccion(int pos) {
        return produccion[pos - 1];
    }

    public void moverCarta(boolean aprobar, String ruta) {
        ma.moverCarta(aprobar, ruta);
        buzon = ma.cargar(ManejadorArchivos.BUZON);
        produccion = ma.cargar(ManejadorArchivos.PRODU);
    }

    public Carta[] getBuzon() {
        return buzon;
    }

    public Carta[] getProduccion() {
        return produccion;
    }

    public String listaJuguetes() {
        String txt = "";
        int i = 1;
        for (Juguete juguete : recetas) {
            if (juguete != null) {
                txt += i++ + ". " + juguete.getCodigo() + " - " + juguete.getNombre() + "\n";
            }
        }
        return txt;
    }

    public Juguete getJuguete(int pos) {
        return recetas[pos - 1];
    }

    public String registrarPedido(Pedido p, Juguete j, Carta c) {
        if (madera < j.getCanMaderaMetal()
                || clavos < j.getCanClaTor()
                || pintura < j.getCanPintura()) {
            return "Insumos insuficientes";
        }

        for (int i = 0; i < pedidos.length; i++) {
            if (pedidos[i] == null) {
                pedidos[i] = p;
                ma.moverCarta(false, c.getRuta());
                produccion = ma.cargar(ManejadorArchivos.PRODU);
                return "Pedido registrado con éxito";
            }
        }
        return "No es posible registrar más pedidos";
    }

    public String getPedidos(char estado) {
        String txt = "";
        int i = 1;
        for (Pedido pedido : pedidos) {
            if (pedido != null && pedido.getEstado() == estado) {
                txt += i + ". " + pedido.getCodigo() + " - " + pedido.getCedula() + "\n";
            }
            i++;
        }
        return txt;
    }

    public void cancelarPedido(int num) {
        pedidos[num - 1] = null;
    }

    public String finalizarPedido(int num) {
        Pedido p = pedidos[num - 1];
        Juguete j = buscarJuguete(p.getCodigo());
        if (j == null) {
            return "Juguete fuera de producción";
        }
        if (madera < j.getCanMaderaMetal()
                || clavos < j.getCanClaTor()
                || pintura < j.getCanPintura()) {
            return "Insumos insuficientes";
        }
        madera -= j.getCanMaderaMetal();
        clavos -= j.getCanClaTor();
        pintura -= j.getCanPintura();
        p.setEstado('F');
        return "Pedido finalizado con éxito";
    }

    private Juguete buscarJuguete(int cod) {
        for (Juguete receta : recetas) {
            if (receta != null && receta.getCodigo() == cod) {
                return receta;
            }
        }
        return null;
    }

    private Nino buscarNino(String ced) {
        for (Nino nino : ninos) {
            if (nino != null && ced.equals(nino.getCedula())) {
                return nino;
            }
        }
        return null;
    }

    public String imprimirColilla(int num) {
        Pedido p = pedidos[num - 1];
        Juguete j = buscarJuguete(p.getCodigo());
        Nino n = buscarNino(p.getCedula());

        String f = "--------------------------------------------\n"
                + "Para:      %s\n"
                + "Juguete:   %s\n"
                + "Dirección: %s\n"
                + "---------------------------------------------";

        p.setEstado('E');
        return String.format(f, n.getNombre(), j.getNombre(), n.getDireccion());
    }

    public int getCantPedidos(char estado) {
        int i = 0;
        for (Pedido pedido : pedidos) {
            if (pedido != null && pedido.getEstado() == estado) {
                i++;
            }
        }
        return i;
    }

    public String consultaProduccion() {
        String txt = "----------------------------------------------\n"
                + "Promedio:\n "
                + "Madera/Metal: %.1f | Clavos/Tornillos: %.1f | Pintura: %.1f\n"
                + "Necesario:\n "
                + "Madera/Metal: %.1f | Clavos/Tornillos: %.1f | Pintura: %.1f\n"
                + "Insumos Actuales:\n "
                + "Madera/Metal: %d | Clavos/Tornillos: %d | Pintura: %d\n\n"
                + "Conclusión: %s";

        float mp = 0;
        float cp = 0;
        float pp = 0;
        String fechas = "";

        for (Pedido pedido : pedidos) {
            if (pedido != null && (pedido.getEstado() == 'F' || pedido.getEstado() == 'E')) {
                Juguete j = buscarJuguete(pedido.getCodigo());
                mp += j.getCanMaderaMetal();
                cp += j.getCanClaTor();
                pp += j.getCanPintura();
                if (!fechas.contains(pedido.getFecha().toString())) {
                    fechas += pedido.getFecha().toString() + ",";
                }
            }
        }
        int cant = fechas.split(",").length;
        mp = mp / cant;
        cp = cp / cant;
        pp = pp / cant;

        float mn = 0;
        float cn = 0;
        float pn = 0;

        for (Pedido pedido : pedidos) {
            if (pedido != null && pedido.getEstado() == 'P') {
                Juguete j = buscarJuguete(pedido.getCodigo());
                mn += j.getCanMaderaMetal();
                cn += j.getCanClaTor();
                pn += j.getCanPintura();
            }
        }

        String resp = "";
        if (mn <= madera && cn <= clavos && pn <= pintura) {
            resp = "Suficientes insumos para concluir con los pendientes\n";
        } else {
            resp = "Es nesario comprar más insumos, para concluir los pendientes\n";
        }
        int dm = (int) (madera / mp);
        int dc = (int) (clavos / cp);
        int dp = (int) (pintura / pp);
        resp += String.format("Madera/Metal: %d días\n"
                + "Clavos/Tornillos: %d días\n"
                + "Pintura: %d días", dm, dc, dp);
        return String.format(txt, mp, cp, pp, mn, cn, pn, madera, clavos, pintura, resp);
    }

}
