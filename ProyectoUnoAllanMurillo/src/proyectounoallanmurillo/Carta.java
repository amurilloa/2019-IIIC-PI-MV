/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

import java.util.Date;

/**
 *
 * @author Allan Murillo
 */
public class Carta {
    
    private Date fecha;
    private String ruta;
    private String contenido;

    public Carta() {
    }

    public Carta(Date fecha, String ruta, String contenido) {
        this.fecha = fecha;
        this.ruta = ruta;
        this.contenido = contenido;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    @Override
    public String toString() {
        return "Carta{" + "fecha=" + fecha + ", ruta=" + ruta + ", contenido=" + contenido + '}';
    }
    
}
