/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectounoallanmurillo;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class ProyectoUnoAllanMurillo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();

        //TODO: Cargar usuarios desde el archivo  --> Constructor logica - Borrar
        //log.registrarUsuario(new Usuario("1", "Allan Murillo", "allanmual@gmail.com", "123", 'A'), true);
        //log.registrarUsuario(new Usuario("2", "Luis Morales", "empleado@gmail.com", "123", 'E'), true);
        log.registrarNino(new Nino("206470568", "María Pérez", "La Fortuna, San Carlos", 'F', Util.textoFecha("23/11/2010")));
        log.registrarPedido(new Pedido(103, "206470568", Util.textoFecha(log.hoy()), 'P'),
                log.getJuguete(3), log.getCartaProduccion(3));

        log.registrarNino(new Nino("206470563", "Juancito Pérez", "La Fortuna, San Carlos", 'M', Util.textoFecha("23/11/2010")));
        log.registrarPedido(new Pedido(101, "206470563", Util.textoFecha("11/11/2019"), 'P'),
                log.getJuguete(3), log.getCartaProduccion(1));

        log.registrarNino(new Nino("206470567", "Luis Rojas", "La Fortuna, San Carlos", 'M', Util.textoFecha("23/10/2013")));
        log.registrarPedido(new Pedido(101, "206470567", Util.textoFecha("10/11/2019"), 'P'),
                log.getJuguete(1), log.getCartaProduccion(1));
        String menu = "1. Iniciar Sesión\n"
                + "2. Registro\n"
                + "3. Salir";

        String menuAdm = "1. Aprobar/Rechazar Cartas\n"
                + "2. Realizar Pedido\n"
                + "3. Cancelar Pedido\n"
                + "4. Consultas\n"
                + "5. Salir";

        String menuAdmAR = "1.Ver cartas del día\n"
                + "2. Ver cartas por fecha\n"
                + "3. Ver cartas pendientes\n"
                + "4. Cancelar";

        String menuAdmC = "1.Producción vs Insumos\n"
                + "2. Artículos Terminados\n"
                + "3. Cancelar";

        String menuEmp = "1. Realizar Pedido\n"
                + "2. Finaliza Pedido\n"
                + "3. Imprimir Colillas\n"
                + "4. Salir";

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    String ced = Util.leerTexto("Cédula");
                    String pas = Util.leerTexto("Contraseña");
                    Usuario user = log.iniciarSesion(ced, pas);
                    if (user != null && user.getTipo() == 'A') {
                        ADM:
                        do {
                            op = Util.leerInt(menuAdm);
                            switch (op) {
                                case 1:
                                    op = Util.leerInt(menuAdmAR);
                                    String cartas;
                                    if (op == 1) {
                                        cartas = log.cargarCartasHoy(log.getBuzon());
                                    } else if (op == 2) {
                                        cartas = log.cargarCartas(Util.textoFecha(Util.leerTexto("Fecha:")), log.getBuzon());
                                    } else if (op == 3) {
                                        cartas = log.cargarCartas(log.getBuzon());
                                    } else {
                                        break ADM;
                                    }
                                    int num = Util.leerInt(cartas);
                                    Carta temp = log.getCartaBuzon(num);
                                    boolean aprobar = Util.confirmar(temp.getContenido()
                                            + "\n¿Desea aprobar la carta, caso contrario se rechaza?");
                                    log.moverCarta(aprobar, temp.getRuta());
                                    break;
                                case 2:
                                    op = Util.leerInt(menuAdmAR);
                                    if (op == 1) {
                                        cartas = log.cargarCartasHoy(log.getProduccion());
                                    } else if (op == 2) {
                                        cartas = log.cargarCartas(Util.textoFecha(Util.leerTexto("Fecha:")), log.getProduccion());
                                    } else if (op == 3) {
                                        cartas = log.cargarCartas(log.getProduccion());
                                    } else {
                                        break ADM;
                                    }
                                    num = Util.leerInt(cartas);
                                    temp = log.getCartaProduccion(num);
                                    //Niño
                                    Nino n = new Nino();
                                    n.setCedula(Util.leerTexto(temp.getContenido() + "\nCédula"));
                                    n.setNombre(Util.leerTexto(temp.getContenido() + "\nNombre"));
                                    n.setFechaNacimiento(Util.textoFecha(Util.leerTexto(temp.getContenido() + "\nFecha")));
                                    n.setDireccion(Util.leerTexto(temp.getContenido() + "\nDirección"));
                                    n.setGenero(Util.seleccionar(temp.getContenido()
                                            + "\nGénero", new String[]{"Masculino", "Femenino"}).charAt(0));
                                    Util.mostrar(log.registrarNino(n));

                                    int j = Util.leerInt(log.listaJuguetes());
                                    Juguete jug = log.getJuguete(j);

                                    Pedido p = new Pedido(jug.getCodigo(), n.getCedula(), Util.textoFecha(log.hoy()), 'P');
                                    Util.mostrar(log.registrarPedido(p, jug, temp));
                                    break;
                                case 3:
                                    num = Util.leerInt(log.getPedidos('P'));
                                    log.cancelarPedido(num);
                                    break;
                                case 4:
                                    op = Util.leerInt(menuAdmC);
                                    if (op == 1) {
                                        Util.mostrar(log.consultaProduccion());
                                    } else if (op == 2) {
                                        Util.mostrar("Cantidad de artículos terminados: "
                                                + log.getCantPedidos('F'));
                                    }
                                    break;
                                case 5:
                                    break ADM;
                            }
                        } while (true);

                    } else if (user != null && user.getTipo() == 'E') {
                        EMP:
                        do {
                            op = Util.leerInt(menuEmp);
                            switch (op) {
                                case 1:
                                    op = Util.leerInt(menuAdmAR);
                                    String cartas;
                                    if (op == 1) {
                                        cartas = log.cargarCartasHoy(log.getProduccion());
                                    } else if (op == 2) {
                                        cartas = log.cargarCartas(Util.textoFecha(Util.leerTexto("Fecha:")), log.getProduccion());
                                    } else if (op == 3) {
                                        cartas = log.cargarCartas(log.getProduccion());
                                    } else {
                                        break EMP;
                                    }
                                    int num = Util.leerInt(cartas);
                                    Carta temp = log.getCartaProduccion(num);
                                    //Niño
                                    Nino n = new Nino();
                                    n.setCedula(Util.leerTexto(temp.getContenido() + "\nCédula"));
                                    n.setNombre(Util.leerTexto(temp.getContenido() + "\nNombre"));
                                    n.setFechaNacimiento(Util.textoFecha(Util.leerTexto(temp.getContenido() + "\nFecha")));
                                    n.setDireccion(Util.leerTexto(temp.getContenido() + "\nDirección"));
                                    n.setGenero(Util.seleccionar(temp.getContenido()
                                            + "\nGénero", new String[]{"Masculino", "Femenino"}).charAt(0));
                                    Util.mostrar(log.registrarNino(n));

                                    int j = Util.leerInt(log.listaJuguetes());
                                    Juguete jug = log.getJuguete(j);

                                    Pedido p = new Pedido(jug.getCodigo(), n.getCedula(), Util.textoFecha(log.hoy()), 'P');
                                    Util.mostrar(log.registrarPedido(p, jug, temp));
                                    break;
                                case 2:
                                    String ped = log.getPedidos('P');
                                    if (ped.length() > 0) {
                                        num = Util.leerInt(ped);
                                        Util.mostrar(log.finalizarPedido(num));
                                    } else {
                                        Util.mostrar("No hay pedidos pendientes");
                                    }
                                    break;
                                case 3:
                                    ped = log.getPedidos('F');
                                    if (ped.length() > 0) {
                                        num = Util.leerInt(ped);
                                        Util.mostrar(log.imprimirColilla(num));
                                    } else {
                                        Util.mostrar("No hay pedidos finalizados");
                                    }
                                    break;
                                case 4:
                                    break EMP;
                            }
                        } while (true);
                    } else {
                        Util.mostrar("Credenciales inválidas!!");
                    }

                    break;
                case 2:
                    Usuario u = new Usuario();
                    u.setCedula(Util.leerTexto("Cédula"));
                    u.setNombre(Util.leerTexto("Nombre"));
                    u.setCorreo(Util.leerTexto("Correo"));
                    u.setContrasena(Util.leerTexto("Contraseña"));
                    u.setTipo(Util.seleccionar("Tipo Usuario", new String[]{"Admin", "Empleado"}).charAt(0));
                    Util.mostrar(log.registrarUsuario(u, true));
                    break;
                case 3:
                    break APP;
            }
        } while (true);

    }

}
