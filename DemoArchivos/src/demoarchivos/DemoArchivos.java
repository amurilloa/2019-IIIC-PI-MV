/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoarchivos;

/**
 *
 * @author Allan Murillo
 */
public class DemoArchivos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();

        String[] generos = {"Masculino", "Femenino"};

        String menu = "1. Aplicar encuesta\n"
                + "2. Mostrar resultados\n"
                + "3. Borrar encuesta\n"
                + "4. Salir";

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    Persona p = new Persona();
                    p.setCedula(Util.leerInt("Cédula"));
                    p.setNombre(Util.leerTexto("Nombre"));
                    p.setGenero(Util.seleccionar("Género", generos).charAt(0));
                    Util.mostrar(log.registrarEncuesta(p));
                    break;
                case 2:
                    Util.mostrar(log.estadisticas());
                    break;
                case 3:
                    int id = Util.leerInt(log.imprimir());
                    log.borrar(id);
                    break;
                case 4:
                    break APP;
            }
        } while (true);
    }

}
