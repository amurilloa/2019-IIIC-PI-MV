/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoarchivos;

/**
 *
 * @author Allan Murillo
 */
public class Persona {

    private int id;
    private int cedula;
    private String nombre;
    private char genero;

    public Persona() {
    }

    public Persona(int id, int cedula, String nombre, char genero) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.genero = genero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return id + "," + cedula + "," + nombre + "," + genero;
    }
}
