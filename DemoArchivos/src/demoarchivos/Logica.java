/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoarchivos;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private final Persona[] encuestas;
    private final ManejadorArchivos ma;

    private final String RUTA = "datos\\datos.txt";

    public Logica() {
        encuestas = new Persona[100];
        ma = new ManejadorArchivos();
        cargarPersonas();
    }

    private void cargarPersonas() {
        String txt = ma.leerTextoArchivo(RUTA);
        String[] personas = txt.split("\n");
        for (String linea : personas) {
            String[] datos = linea.split(",");
            Persona p = new Persona();
            p.setId(Integer.parseInt(datos[0]));
            p.setCedula(Integer.parseInt(datos[1]));
            p.setNombre(datos[2]);
            p.setGenero(datos[3].charAt(0));
            encuestas[p.getId()] = p;
        }
    }

    public String registrarEncuesta(Persona p) {
        for (int i = 0; i < encuestas.length; i++) {
            if (encuestas[i] == null) {
                p.setId(i);
                encuestas[i] = p;
                guardarPersonas();
                return "Encuesta registrada con éxito";
            }
        }
        return "No hay espacios para más encuestas";
    }

    public void guardarPersonas() {
        String txt = "";
        for (Persona encuesta : encuestas) {
            if (encuesta != null) {
                txt += encuesta + "\n";
            }
        }
        ma.escribirTextoArchivo(RUTA, txt);
    }

    public String estadisticas() {
        int hom = 0;
        int muj = 0;

        for (Persona encuesta : encuestas) {
            if (encuesta != null) {
                if (encuesta.getGenero() == 'M') {
                    hom++;
                } else {
                    muj++;
                }
            }
        }

        int total = hom + muj;
        String res = "Total: %d\n"
                + "Total Hombres: %d(%.2f%%)\n"
                + "Total Mujeres: %d(%.2f%%)";
        return String.format(res, total, hom, hom * 100.0 / total, muj, muj * 100.0 / total);
    }

    public String imprimir() {
        String txt = "";
        for (Persona encuesta : encuestas) {
            if (encuesta != null) {
                txt += encuesta + "\n";
            }
        }
        return txt;
    }

    public void borrar(int id) {
        encuestas[id] = null;
        guardarPersonas();
    }

}
