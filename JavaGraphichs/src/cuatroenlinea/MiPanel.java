/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Tablero tab;
    private Ficha ficha;
    private int numJug;
    private boolean soltar;

    public MiPanel() {
        setBackground(Color.WHITE);
        setFocusable(true);
        addKeyListener(this);
        tab = new Tablero();
        numJug = 1;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(900, 700);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;

        tab.pintar(g2);

        if (soltar) {
            ficha.pintar(g2);
            ficha.mover();
            int res = tab.detener(ficha);
            if (res == 2) {
                numJug = numJug == 1 ? 2 : 1;
            }
            soltar = res == 1;
        }

        g.setColor(Color.RED);
        g.drawLine(0, cY, getWidth(), cY);
        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT && !soltar) {
            tab.cambiarPos(-1);
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT && !soltar) {
            tab.cambiarPos(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_SPACE && !soltar) {
            ficha = new Ficha(105 + tab.getPos() * 100, -90, numJug);
            soltar = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
