/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Tablero {

    private int x;
    private int y;
    private int pos;
    private final Ficha[][] tab;
    
    public Tablero() {
        tab = new Ficha[6][7];
        x = 100;
        y = 20;
    }

    public void pintar(Graphics g) {
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x + 0, y + 0, 700, 660);

        //Huecos y Fichas
        for (int f = 0; f < tab.length; f++) {
            for (int c = 0; c < tab[f].length; c++) {
                if (tab[f][c] != null) {
                    tab[f][c].pintar(g);
                } else {
                    g.setColor(Color.WHITE);
                    g.fillOval(x + 5 + (c * 100), y + 65 + f * 100, 90, 90);
                }
            }
        }

        g.setColor(Color.RED);
        int[] xs = {x + 25 + pos * 100, x + 75 + pos * 100, x + 50 + pos * 100};
        int[] ys = {y + 10, y + 10, y + 55};
        g.fillPolygon(xs, ys, xs.length);

        g.setColor(Color.WHITE);
        Font f = new Font("Arial", Font.BOLD, 22);
        g.setFont(f);
        for (int i = 0; i < 7; i++) {
            
            g.drawString(String.valueOf(i + 1), x + 43 + i * 100, y + 35);
        }
    }

    public void cambiarPos(int num) {
        pos += num;
        if (pos < 0 || pos > 6) {
            pos -= num;
        }
    }

    public int getPos() {
        return pos;
    }

    public int detener(Ficha ficha) {
        int col = pos;
        int fil = -1;
        for (int f = tab.length - 1; f >= 0; f--) {
            if (tab[f][col] == null) {
                fil = f;
                break;
            }
        }
        if (fil < 0) {
            ficha.setDir(0); //Detiene la ficha
            return 0; //Sin campo disponible
        }

        int limY = y + 65 + fil * 100;
        if (ficha.getY() >= limY) {
            ficha.setDir(0);
            tab[fil][col] = ficha;
            return 2;
        }

        return 1; //Viene en movimiento
    }

}
