/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Ficha {

    private int x;
    private int y;
    private int dir;
    private final int numJug;
    private final Color color;

    public Ficha(int x, int y, int numJug) {
        dir = 5;
        this.x = x;
        this.y = y;
        this.numJug = numJug;
        color = numJug == 1 ? Color.BLUE : Color.YELLOW;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, 93, 93);
    }

    public void mover() {
        if (dir == 5) {
            y += 3;
        }
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }
    
    public void setDir(int dir) {
        this.dir = dir;
    }

    
    
}
