/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batmanminion;

import hombrenieve.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel {

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(400,500);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int x = 200;
        int y = 250;

        //Fondo
        g.setColor(Color.white);
        g.fillRect(0, 0, 400, 500);
        
        g.setColor(Color.BLACK);
        g.fillArc(50, 60, 300, 400, 50, 75); //Arco de la cabeza 
        g.fillOval(95, 230, 210, 200); // Circulo del cuerpo
        g.setColor(new Color(244, 202, 82));
        g.fillRect(95, 230, 210, 100); //Parte amarilla de la boca
        g.setColor(Color.black);
        int[] xs = {200, 305, 305, 200, 95, 95};
        int[] ys = {250, 230, 30, 200, 30, 230};
        g.fillPolygon(xs, ys, xs.length); //Orejas con boca
        g.setColor(new Color(189, 190, 192));
        g.fillOval(130, 90, 140, 140); //Cara
        g.setColor(new Color(254, 254, 254));
        g.fillOval(140, 100, 120, 120);//Visor de la cara
        g.setColor(new Color(99, 55, 18));
        g.fillOval(170, 130, 60, 60); //Ojo
        g.setColor(Color.black);
        g.fillOval(185, 145, 30, 30); //Ojo
        int[] x2 = {200, 305, 305, 95, 95};
        int[] y2 = {300, 260, 340, 340, 260};
        g.fillPolygon(x2, y2, x2.length); //Traje o saco
        g.setColor(new Color(251, 242, 0));
        g.fillOval(160, 315, 80, 30); //hebilla de la faja

        g.setColor(Color.BLACK);
        g.fillOval(206, 310, 11, 15);
        g.fillOval(183, 310, 11, 15);
        g.fillOval(197, 312, 6, 8);

        
        
        
        g.setColor(Color.red);
        g.drawLine(0, y, 400, y); //x
        g.drawLine(x, 0, x, 500); //y

    }

}
