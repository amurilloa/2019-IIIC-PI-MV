/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package choques;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Allan Murillo
 */
public class Bola {

    private int x;
    private int y;
    private int dir;
    private Color color;

    private final int TAMANO = 36;
    private final int SPEED = 3;

    public Bola() {
    }

    public Bola(int x, int y, int dir, Color color) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.color = color;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, TAMANO, TAMANO);

        //Calcular bounds
//        g.setColor(Color.MAGENTA);
//        g.drawRect(x + 2, y + 2, TAMANO - 4, TAMANO - 4);

    }

    public Rectangle getBounds() {
        return new Rectangle(x + 2, y + 2, TAMANO - 4, TAMANO - 4);
    }

    public void mover(int ancho) {
        if (dir == 1) {
            y -= SPEED;
        } else if (dir == 2) {
            y -= SPEED;
            x += SPEED;
        } else if (dir == 3) {
            x += SPEED;
        } else if (dir == 4) {
            y += SPEED;
            x += SPEED;
        } else if (dir == 5) {
            y += SPEED;
        } else if (dir == 6) {
            y += SPEED;
            x -= SPEED;
        } else if (dir == 7) {
            x -= SPEED;
        } else if (dir == 8) {
            y -= SPEED;
            x -= SPEED;
        }

        if (x <= 0) {
            dir = dir == 8 ? 2 : 4;
        } else if (x + TAMANO >= ancho) {
            dir = dir == 2 ? 8 : 6;
        } else if (y <= 0) {
            if (dir == 8) {
                dir = 6;
            } else if (dir == 2) {
                dir = 4;
            } else {
                dir = (int) (Math.random() * 3) + 4;
            }
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    @Override
    public String toString() {
        return "Bola{" + "x=" + x + ", y=" + y + ", color=" + color + ", TAMANO=" + TAMANO + '}';
    }

    public void choco(Paleta paleta) {
        if (getBounds().intersects(paleta.getBounds(2))) {
            dir = 1;
        } else if (getBounds().intersects(paleta.getBounds(1))) {
            dir = 8;
        } else if (getBounds().intersects(paleta.getBounds(3))) {
            dir = 2;
        }
    }

}
