/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package choques;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 *
 * @author Allan Murillo
 */
public class Paleta {

    private int x;
    private int y;
    private int dir;
    private final int SPEED = 4;

    public Paleta() {
    }

    public Paleta(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics2D g) {

        g.setColor(Color.DARK_GRAY);
        g.fillRoundRect(x, y, 30, 20, 20, 20);
        g.setColor(Color.DARK_GRAY);
        g.fillRoundRect(x + 50, y, 30, 20, 20, 20);
        g.setColor(Color.RED);
        g.fillRoundRect(x + 20, y, 40, 20, 5, 20);

        //Calculo del bounds
//        g.setColor(Color.MAGENTA);
//        g.drawRect(x+1, y+1, 10, 18);
//        g.drawRect(x + 18, y-1, 44, 22);
//        g.drawRect(x + 69, y+1, 10, 18);

    }

    public Rectangle getBounds(int numParte) {
        if (numParte == 1) {
            return new Rectangle(x+1, y+1, 10, 18);
        } else if (numParte == 2) {
            return new Rectangle(x + 18, y-1, 44, 22);
        } else {
            return new Rectangle(x + 69, y+1, 10, 18);
        }
    }

    public void mover(int ancho) {
        int xTemp = x; //actual

        if (dir == 3) {
            x += SPEED;
        } else if (dir == 7) {
            x -= SPEED;
        }

        if (x <= 0) {
            x = xTemp;
        } else if (x + 80 >= ancho) {
            x = xTemp;
        }

    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getX() {
        return x;
    }

}
