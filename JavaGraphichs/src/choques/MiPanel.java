/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package choques;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Bola bola;
    private Paleta paleta;
    private boolean f1;
    private boolean f2;
    private boolean inicio;
    private boolean perdio;

    public MiPanel() {
        setBackground(Color.WHITE);
        bola = new Bola(232, 544, 0, Color.BLUE);
        paleta = new Paleta(210, 580);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 600);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;

        bola.pintar(g);
        paleta.pintar(g);
        paleta.mover(getWidth());

        if (perdio) {
            f = new Font("DUMMY", Font.ITALIC, 14);
            g.setFont(f);
            g.drawString("<< Perdiste >>", cX - 45, cY);
        } else if (!inicio) {
            bola.setX(paleta.getX() + 22);
        } else {
            bola.mover(getWidth());
            bola.choco(paleta);
            if (bola.getY() >= getHeight()) {
                perdio = true;
            }
        }

        

        g.setColor(Color.RED);
        g.drawLine(0, cY, getWidth(), cY);
        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {

        if (ke.getKeyCode() == KeyEvent.VK_ENTER && perdio) {
            perdio = false;
            inicio = false;
            bola = new Bola(232, 544, 0, Color.BLUE);
            paleta = new Paleta(210, 580);
        }
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            paleta.setDir(3);
            f1 = true;
        }

        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            paleta.setDir(7);
            f2 = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_SPACE && !inicio) {
            inicio = true;
            bola.setDir(1);
        }

        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            f1 = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            f2 = false;
        }
        if (!f1 && !f2) {
            paleta.setDir(0);
        }

    }

}
