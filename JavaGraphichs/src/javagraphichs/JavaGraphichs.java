/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphichs;

import javax.swing.JFrame;

/**
 *
 * @author Allan Murillo
 */
public class JavaGraphichs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frm = new JFrame("Java Graphics - UTN v0.1"); //Crea el formulario
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Cambia la acción por defecto de la X
        frm.add(new MiPanel());
        frm.pack();
        //frm.setSize(300, 400);
        frm.setLocationRelativeTo(null);
        frm.setVisible(true); //Nos muestra el formulario
    }
    
}
