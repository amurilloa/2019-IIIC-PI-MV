/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphichs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel {

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 400);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(new Color(134, 133, 135));
        g2.drawLine(150, 0, 150, 400);
        g2.drawLine(0, 200, 300, 200);
        g2.setColor(Color.BLUE);
        g2.drawOval(100, 150, 100, 100);
        g2.fillOval(145, 195, 10, 10);
        g2.drawRect(100, 50, 100, 50);
        g2.setColor(Color.GREEN);
        g2.fillOval(240, 340, 50, 50);
        g2.setColor(Color.RED);
        g2.fillArc(240, 340, 50, 50, 0, 75);
        int[] xs ={25,75,50};
        int[] ys ={390,390,340};
        g2.setColor(Color.DARK_GRAY);
        g2.fillPolygon(xs, ys, xs.length);
        g2.setColor(Color.CYAN);
        g2.fillOval(10, 10, 75, 75);
        g2.setColor(new Color(238, 238, 238));
        g2.fillOval(20, 20, 55, 55);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int ro = (int) (Math.random() * 255);
                int gr = (int) (Math.random() * 255);
                int bl = (int) (Math.random() * 255);
                g2.setColor(new Color(ro, gr, bl));
                g.drawOval(10 * i, 10 * i, 50 * j, 50 * j);
            }
        }

    }

}
