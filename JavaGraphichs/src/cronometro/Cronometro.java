/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Cronometro {

    private int x;
    private int y;
    private int seg;
    private Numero[] numeros;
    private boolean sombra;
    private int estilo;
    private boolean run;

    public Cronometro() {
        config();
        estilo = 1;
        sombra = true;
    }

    private void config() {
        numeros = new Numero[6];
        int sep = 0;
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = new Numero(x + 115 + sep + i * 100, y + 295);
            numeros[i].sombra();
            if (i % 2 != 0) {
                sep += 40;
            }
        }
    }

    public void pintar(Graphics g) {
        calcularTiempo();
        int st = seg % 60;
        int gra = st * 360 / 60;
        gra = st == 0 ? 360 : gra;
        g.setColor(Color.YELLOW);
        g.drawOval(x, y , 900, 750);
        g.fillArc(x, y, 900, 750, 90, -gra);

        g.setColor(Color.BLACK);
        g.fillOval(x + 15, y + 10, 870, 730);
        for (int i = 0; i < numeros.length; i++) {
            numeros[i].setEstilo(estilo);
            numeros[i].pintar(g);
        }
        g.setColor(estilo == 1 ? Color.CYAN : Color.GREEN);
        g.fillOval(x + 320, y + 325, 18, 18);
        g.fillOval(x + 320, y + 390, 18, 18);
        g.fillOval(x + 560, y + 325, 18, 18);
        g.fillOval(x + 560, y + 390, 18, 18);

    }

    public void sumarSegundo() {
        if (run) {
            seg++;
        }
    }

    private void calcularTiempo() {
        int st = seg % 60;
        int mt = seg / 60;
        int ht = mt / 60;
        mt = mt % 60;

        numeros[5].setNum(st % 10);
        numeros[4].setNum(st / 10);
        numeros[3].setNum(mt % 10);
        numeros[2].setNum(mt / 10);
        numeros[1].setNum(ht % 10);
        numeros[0].setNum(ht / 10);
    }

    public void setEstilo(int estilo) {
        this.estilo = estilo;
    }

    public void sombra() {
        for (int i = 0; i < numeros.length; i++) {
            numeros[i].sombra();
        }
    }

    public void start() {
        run = true;
    }

    public void pause() {
        run = !run;
    }

    public void stop() {
        run = false;
        seg = 0;
    }

}
