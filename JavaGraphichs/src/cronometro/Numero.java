/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Numero {

    private int x;
    private int y;
    private int num;
    private boolean sombra;
    private int estilo;

    private final Color SOMBRA = new Color(50, 50, 50);

    public Numero() {
    }

    public Numero(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics g) {
        if (estilo == 1) {
            pintarLED(g);
        } else {
            pintarDefecto(g);
        }
    }

    private int[] inc(int[] arr, int valor) {
        int[] temp = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i] + valor;
        }
        return temp;
    }

    public void setNum(int num) {
        if (num >= 0 && num <= 9) {
            this.num = num;
        }
    }

    private void pintarDefecto(Graphics g) {

        int[] arr1 = {5, 10, 70, 75, 70, 10};
        int[] arr2 = {5, 0, 0, 5, 10, 10};
        if (sombra) {
            g.setColor(SOMBRA);
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y), arr1.length); // Arriba
            g.fillPolygon(inc(arr2, x), inc(arr1, y + 2), arr2.length); // Izq-Arr
            g.fillPolygon(inc(arr2, x + 74), inc(arr1, y + 2), arr2.length); // Der-Arr
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y + 74), arr1.length); // Centro
            g.fillPolygon(inc(arr2, x), inc(arr1, y + 76), arr2.length); // Izq-Aba
            g.fillPolygon(inc(arr2, x + 74), inc(arr1, y + 76), arr2.length); // Der-Abj
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y + 148), arr1.length); // Abj
        }

        g.setColor(Color.GREEN);
        if (num != 1 && num != 4) {
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y), arr1.length); // Arriba
        }

        if (num != 1 && num != 2 && num != 3 && num != 7) {
            g.fillPolygon(inc(arr2, x), inc(arr1, y + 2), arr2.length); // Izq-Arr
        }

        if (num != 5 && num != 6) {
            g.fillPolygon(inc(arr2, x + 74), inc(arr1, y + 2), arr2.length); // Der-Arr
        }

        if (num != 0 && num != 1 && num != 7) {
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y + 74), arr1.length); // Centro
        }

        if (num == 0 || num == 2 || num == 6 || num == 8) {
            g.fillPolygon(inc(arr2, x), inc(arr1, y + 76), arr2.length); // Izq-Aba
        }

        if (num != 2) {
            g.fillPolygon(inc(arr2, x + 74), inc(arr1, y + 76), arr2.length); // Der-Abj
        }

        if (num != 1 && num != 4 && num != 7) {
            g.fillPolygon(inc(arr1, x + 2), inc(arr2, y + 148), arr1.length); // Abj
        }
    }

    public void sombra() {
        sombra = !sombra;
    }

    private void pintarLED(Graphics g) {
        boolean[][] leds = new boolean[9][5];

        if (num != 1 && num != 4) {
            linea(0, leds);
        }

        if (num != 1 && num != 2 && num != 3 && num != 7) {
            columna(0, 0, leds);
        }

        if (num != 5 && num != 6) {
            columna(0, 4, leds);
        }

        if (num != 0 && num != 1 && num != 7) {
            linea(4, leds);
        }

        if (num == 0 || num == 2 || num == 6 || num == 8) {
            columna(4, 0, leds);
        }

        if (num != 2) {
            columna(4, 4, leds);
        }

        if (num != 1 && num != 4 && num != 7) {
            linea(8, leds);
        }

        for (int f = 0; f < leds.length; f++) {
            for (int c = 0; c < leds[f].length; c++) {
                g.setColor(leds[f][c] ? Color.CYAN : sombra ? SOMBRA : Color.BLACK);
                g.fillOval(x + 1 + c * 17, y + f * 18, 15, 15);
            }
        }
    }

    private void linea(int fila, boolean[][] leds) {
        for (int c = 0; c < leds[fila].length; c++) {
            leds[fila][c] = true;
        }
    }

    private void columna(int fila, int col, boolean[][] leds) {
        for (int f = fila; f < fila + 5; f++) {
            leds[f][col] = true;
        }
    }

    public void setEstilo(int estilo) {
        this.estilo = estilo;
    }
}
