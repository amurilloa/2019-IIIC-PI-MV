/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Cronometro cro;
    private int ciclos;

    public MiPanel() {
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
        cro = new Cronometro();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(900, 750);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;
        ciclos++;
        cro.pintar(g);
        if (ciclos >= 100) {
            cro.sumarSegundo();
            ciclos = 0;
        }
        g.setColor(Color.RED);
//        g.drawLine(0, cY, getWidth(), cY);
//        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_D) {
            cro.setEstilo(0);
        } else if (ke.getKeyCode() == KeyEvent.VK_L) {
            cro.setEstilo(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_S) {
            cro.sombra();
        } else if (ke.getKeyCode() == KeyEvent.VK_I) {
            ciclos = 0;
            cro.start();
        } else if (ke.getKeyCode() == KeyEvent.VK_P) {
            ciclos = 0;
            cro.pause();
        }else if (ke.getKeyCode() == KeyEvent.VK_O) {
            cro.stop();
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
