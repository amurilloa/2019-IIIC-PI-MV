/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimiento;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Bola b1;
    private Bola b2;
    private boolean pausa;
    private boolean jugando;
    private boolean salir;
    private int tiempo;

    public MiPanel() {
        setBackground(Color.WHITE);
        //setBorder(new LineBorder(Color.RED, 1));
        b1 = new Bola(232, 400, 0, Color.BLUE);
        b2 = new Bola(232, 400, 0, Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 600);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;

        if (salir) {
            f = new Font("DUMMY", Font.BOLD, 24);
            g.setColor(Color.BLACK);
            g.setFont(f);
            g.drawString("EXIT", cX - 42, cY);
            f = new Font("DUMMY", Font.ITALIC, 14);
            g.setFont(f);
            g.drawString("<< Press Y for Exit or N for Resume>>", cX - 135, cY + 30);
        } else {
            g.setColor(Color.BLACK);
            f = new Font("Arial", Font.BOLD, 12);
            g.setFont(f);
            g.drawString("Exit (E)", getWidth() - 45, 15);
            if (!jugando && !pausa) {
                f = new Font("DUMMY", Font.BOLD, 24);
                g.setColor(Color.BLACK);
                g.setFont(f);
                g.drawString("PLAY", cX - 42, cY);
                f = new Font("DUMMY", Font.ITALIC, 14);
                g.setFont(f);
                g.drawString("<< Press Enter for play >>", cX - 95, cY + 30);
            } else {
                if (jugando) {
                    tiempo += 20;
                }
                g.setColor(Color.BLACK);
                f = new Font("Arial", Font.BOLD, 12);
                g.setFont(f);
                g.drawString(String.valueOf(tiempo / 1000), getWidth() - 35, 30);
                if (!pausa) {
                    b1.mover(getWidth(), getHeight());
                    b2.mover(getWidth(), getHeight());
                    b1.pintar(g2);
                    b2.pintar(g2);
                } else {
                    b1.pintarP(g2);
                    b2.pintarP(g2);
                    f = new Font("DUMMY", Font.BOLD, 24);
                    g.setColor(Color.BLACK);
                    g.setFont(f);
                    g.drawString("PAUSA", cX - 42, cY);
                    f = new Font("DUMMY", Font.ITALIC, 14);
                    g.setFont(f);
                    g.drawString("<< Press P for resume >>", cX - 85, cY + 30);
                }
            }
        }

        //Lineas guías
//        g.setColor(Color.RED);
//        g.drawLine(0, cY, getWidth(), cY);
//        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
            b1.setDir(5);
            b2.setDir(5);
            pausa = false;
            jugando = true;
        } else if (ke.getKeyCode() == KeyEvent.VK_R) {
            b1 = new Bola(232, 400, 0, Color.BLUE);
            b2 = new Bola(232, 400, 0, Color.BLACK);
            tiempo = 0;
            jugando = false;
            pausa = false;
        } else if (ke.getKeyCode() == KeyEvent.VK_P) {
            pausa = !pausa;
            jugando = !pausa;
        } else if (ke.getKeyCode() == KeyEvent.VK_E) {
            salir = true;
        } else if (salir && ke.getKeyCode() == KeyEvent.VK_Y) {
            System.exit(0);
        } else if (salir && ke.getKeyCode() == KeyEvent.VK_N) {
            salir = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
