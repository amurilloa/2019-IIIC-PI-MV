/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimiento;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Bola {

    private int x;
    private int y;
    private int dir;
    private Color color;

    private final int TAMANO = 36;
    private final int SPEED = 3;

    public Bola() {
    }

    public Bola(int x, int y, int dir, Color color) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.color = color;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, TAMANO, TAMANO);
    }

    public void pintarP(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillOval(x - 2, y - 2, TAMANO + 4, TAMANO + 4);
        g.setColor(color);
        g.fillOval(x, y, TAMANO, TAMANO);
    }

    public void mover(int ancho, int alto) {
        if (dir == 1) {
            y -= SPEED;
        } else if (dir == 2) {
            y -= SPEED;
            x += SPEED;
        } else if (dir == 3) {
            x += SPEED;
        } else if (dir == 4) {
            y += SPEED;
            x += SPEED;
        } else if (dir == 5) {
            y += SPEED;
        } else if (dir == 6) {
            y += SPEED;
            x -= SPEED;
        } else if (dir == 7) {
            x -= SPEED;
        } else if (dir == 8) {
            y -= SPEED;
            x -= SPEED;
        }

        if (x <= 0) {
            dir = cambiarDir(7);
        } else if (x + TAMANO >= ancho) {
            dir = cambiarDir(3);
        } else if (y <= 0) {
            dir = cambiarDir(1);
        } else if (y + TAMANO >= alto) {
            dir = cambiarDir(5);
        }
    }

    private int cambiarDir(int pared) {
        int r = (int) (Math.random() * 3); //0, 1, 2
        int[] ds = {0, 0, 0};

        if (dir == 1) {
            ds = new int[]{4, 5, 6};
        } else if (dir == 2) {
            if (pared == 1) {
                ds = new int[]{4, 5, 4};
            } else {
                ds = new int[]{8, 7, 8};
            }
        } else if (dir == 3) {
            ds = new int[]{6, 7, 8};
        } else if (dir == 4) {
            if (pared == 3) {
                ds = new int[]{6, 7, 6};
            } else {
                ds = new int[]{2, 1, 2};
            }
        } else if (dir == 5) {
            ds = new int[]{8, 1, 2};
        } else if (dir == 6) {
            if (pared == 5) {
                ds = new int[]{8, 1, 8};
            } else {
                ds = new int[]{4, 3, 4};
            }
        } else if (dir == 7) {
            ds = new int[]{2, 3, 4};
        } else {
            if (pared == 7) {
                ds = new int[]{2, 3, 2};
            } else {
                ds = new int[]{6, 5, 6};
            }
        }
        return ds[r];
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    @Override
    public String toString() {
        return "Bola{" + "x=" + x + ", y=" + y + ", color=" + color + ", TAMANO=" + TAMANO + '}';
    }

}
