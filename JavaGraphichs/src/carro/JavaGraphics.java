/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import javax.swing.JFrame;

/**
 *
 * @author Allan Murillo
 */
public class JavaGraphics {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame("Java Graphics - UTN v.01"); //Se crea el formulario
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Se asigna la operación de salir de la aplicación cuando se presiona la X 
        frm.add(new MiPanel());//agregar un panel al formulario 
        frm.pack(); //Validar tamaños y ajustar la ventana a los componentes
        frm.setLocationRelativeTo(null); //Centramos la ventana 
        frm.setVisible(true);//Mostramos el formulario
        while(true){
            Thread.sleep(20);
            frm.repaint();
        }
    }
    
}
