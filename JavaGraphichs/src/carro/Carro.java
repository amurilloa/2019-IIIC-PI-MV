/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Carro {

    private int placa;
    private String marca;
    private boolean encedido;
    private int direccion;

    private Color colorP;
    private Color colorS;
    private int x;
    private int y;

    public Carro() {
    }

    public Carro(int placa, String marca, boolean encedido, Color colorP, Color colorS, int x, int y) {
        this.placa = placa;
        this.marca = marca;
        this.encedido = encedido;
        this.colorP = colorP;
        this.colorS = colorS;
        this.x = x;
        this.y = y;
    }

    public void dibujar(Graphics g) {
        //Carroceria
        int[] xs = {x + 30, x + 50, x + 120, x + 150, x + 195, x + 195, x + 30};
        int[] ys = {y + 20, y + 0, y + 0, y + 15, y + 20, y + 40, y + 40};
        g.setColor(colorP);
        g.fillPolygon(xs, ys, xs.length);

        //Llantas
        g.setColor(colorS);
        g.fillOval(x + 50, y + 25, 30, 30);
        g.fillOval(x + 150, y + 25, 30, 30);

        //Luces
        g.setColor(Color.RED);
        g.fillRect(x + 31, y + 21, 4, 8);
        g.setColor(encedido ? Color.YELLOW : Color.WHITE);
        g.fillRect(x + 189, y + 21, 5, 5);

        //Mufla
        g.setColor(Color.GRAY);
        g.fillRect(x + 25, y + 38, 15, 4);

        //Humo
        if (encedido) {
            g.setColor(Color.DARK_GRAY);
            g.fillOval(x + 16, y + 38, 8, 4);
            g.fillOval(x + 12, y + 36, 8, 4);
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setEncedido(boolean encedido) {
        this.encedido = encedido;
    }
    
    

}
