/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel {

    private Carro c1;
    private Carro c2;

    public MiPanel() {
        c1 = new Carro(12354, "Toyota", true, Color.BLUE, Color.BLACK, 0, 100);
        c2 = new Carro(12354, "Toyota", true, Color.PINK, Color.GREEN, 0, 200);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 600);

    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;

        c1.dibujar(g2);
        c1.setX(c1.getX() + 2);
        if (c1.getX() >= getWidth()) {
            c1.setX(-200);
        }
        c2.dibujar(g2);
        if (c2.getX() + 195 >= getWidth()) {
            c2.setEncedido(false);
        } else {
            c2.setX(c2.getX() + 3);
        }
        //Lineas guías
        g.setColor(Color.RED);
        g.drawLine(0, cY, getWidth(), cY);
        g.drawLine(cX, 0, cX, getHeight());

    }

}
