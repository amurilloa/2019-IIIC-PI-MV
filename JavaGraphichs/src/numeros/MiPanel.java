/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeros;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Numero num;

    public MiPanel() {
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
        num = new Numero(157, 71);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(400, 300);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;
//        num2.pintar(g2);
        num.pintar(g);
//        g.setColor(Color.RED);
//        g.drawLine(0, cY, getWidth(), cY);
//        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        num.setNum(ke.getKeyCode() - 48);
        if (KeyEvent.VK_S == ke.getKeyCode()) {
            num.sombra();
        } else if (KeyEvent.VK_D == ke.getKeyCode()) {
            num.setEstilo(0);
        } else if (KeyEvent.VK_L == ke.getKeyCode()) {
            num.setEstilo(1);
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
