/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Puntero {

    private int x;
    private int y;
    private int fila;
    private int columna;
    private final int LIM_COL = 3;
    private final int LIM_FIL = 3;

    public Puntero() {
        y = 150;
        x = 20;
        fila = 0;
        columna = 0;
    }

    public int getColumna() {
        return columna;
    }

    public int getFila() {
        return fila;
    }

    public void pintar(Graphics g) {
        g.setColor(Color.RED);
        int dx =  columna * 104;
        int dy = fila * 104;
        int[] xs = {x + 20 + dx, x + 20 + dx, x + 13 + dx, x + 6 + dx, x + 0 + dx, x + 7 + dx, x + 0 + dx};
        int[] ys = {y + 0 + dy, y + 30 + dy, y + 24 + dy, y + 38 + dy, y + 33 + dy, y + 20 + dy, y + 15 + dy};
        //desplazar(xs, dx);
        //desplazar(ys, dy);
        g.fillPolygon(xs, ys, xs.length);
    }

    private void desplazar(int[] arr, int inc) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] += inc;
        }
    }

    public void cambiarFila(int dir) {
        fila += dir;
        if (fila < 0 || fila >= LIM_FIL) {
            fila -= dir;
        }
    }

    public void cambiarColumna(int dir) {
        columna += dir;
        if (columna < 0 || columna >= LIM_COL) {
            columna -= dir;
        }
    }

}
