/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import cuatroenlinea.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Logica gato;
    private Puntero puntero;

    public MiPanel() {
        puntero = new Puntero();
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
        gato = new Logica();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 400);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;
        gato.pintar(g);
        puntero.pintar(g);
//        g.setColor(Color.RED);
//        g.drawLine(0, cY + 50, getWidth(), cY + 50);
//        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            gato.marcar(puntero.getFila() + 1, puntero.getColumna() + 1);
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            puntero.cambiarFila(-1);
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            puntero.cambiarFila(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            puntero.cambiarColumna(-1);
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            puntero.cambiarColumna(1);
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
