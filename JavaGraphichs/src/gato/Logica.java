/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private String j1;
    private String j2;
    private boolean turno;
    private final char[][] tablero;
    private final char MARCA_J1 = 'X';
    private final char MARCA_J2 = 'O';

    private char gano;
    private int filCol;

    private boolean reiniciar;

    public Logica() {
        j1 = "Jugador 1";
        j2 = "Jugador 2";
        tablero = new char[3][3];
        limpiar();
        rifar();
    }

    private void rifar() {
        turno = (int) (Math.random() * 10) + 1 < 5;
    }

    private void limpiar() {
        for (int fil = 0; fil < tablero.length; fil++) {
            for (int col = 0; col < tablero[fil].length; col++) {
                tablero[fil][col] = '_';
            }
        }
    }

    public void pintar(Graphics g) {

        g.setColor(Color.WHITE);
        g.fillRect(0, 100, 300, 300);

        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {

                g.setColor(Color.DARK_GRAY);
                g.fillRect(c * 104, 100 + f * 104, 93, 93);

                Font font = new Font("ARIAL", Font.BOLD, 80);
                g.setFont(font);
                if (tablero[f][c] == 'X') {
                    g.setColor(Color.CYAN);
                    g.drawString("X", c * 104 + 24, 100 + f * 104 + 75);
                } else if (tablero[f][c] == 'O') {
                    g.setColor(Color.PINK);
                    g.drawString("O", c * 104 + 15, 100 + f * 104 + 75);
                }

            }
        }
        Font font = new Font("ARIAL", Font.BOLD, 20);
        g.setFont(font);
        g.setColor(Color.WHITE);

        if (gano()) {
            g.drawString("Ganador: " + getGanador(), 35, 60);
            linea(g);
            reiniciar = true;
        } else if (empate()) {
            g.drawString("Juego empatado", 67, 60);
            reiniciar = true;
        } else {
            //Jugador Actual
            g.drawString(getJugadorActual(), 87, 60);
        }
    }

    private void linea(Graphics g) {
        if (gano == 'F') {
            g.setColor(tablero[filCol][0] == 'X' ? Color.CYAN : Color.PINK);
            g.fillRect(10, 142 + filCol * 104, 281, 10);
        } else if (gano == 'C') {
            g.setColor(tablero[0][filCol] == 'X' ? Color.CYAN : Color.PINK);
            g.fillRect(44 + filCol * 104, 110, 10, 281);
        } else if (gano == 'D') {
            g.setColor(tablero[0][0] == 'X' ? Color.CYAN : Color.PINK);
            int[] xs = {10, 24, 291, 277};
            int[] ys = {114, 114, 385, 385};
            g.fillPolygon(xs, ys, xs.length);
        } else if (gano == 'd') {
            g.setColor(tablero[0][2] == 'X' ? Color.CYAN : Color.PINK);
            int[] xs = {291, 277, 10, 24};
            int[] ys = {114, 114, 385, 385};
            g.fillPolygon(xs, ys, xs.length);
        }

    }

    public void marcar(int fila, int columna) {
        if (reiniciar) {
            limpiar();
            rifar();
            reiniciar = false;
        } else if (fila > 0 && fila < 4 && columna > 0 && columna < 4) {
            if (tablero[fila - 1][columna - 1] == '_') {
                tablero[fila - 1][columna - 1] = turno ? MARCA_J1 : MARCA_J2;
                turno = !turno;
            }
        }
    }

    public boolean gano() {
        //Filas
        for (int f = 0; f < 3; f++) {
            if (tablero[f][0] != '_'
                    && tablero[f][0] == tablero[f][1]
                    && tablero[f][0] == tablero[f][2]) {
                gano = 'F';
                filCol = f;
                return true;
            }
        }
        //Columnas
        for (int c = 0; c < 3; c++) {
            if (tablero[0][c] != '_'
                    && tablero[0][c] == tablero[1][c]
                    && tablero[0][c] == tablero[2][c]) {
                gano = 'C';
                filCol = c;
                return true;
            }
        }

        if (tablero[0][0] != '_'
                && tablero[0][0] == tablero[1][1]
                && tablero[0][0] == tablero[2][2]) {
            gano = 'D';
            return true;
        }

        if (tablero[0][2] != '_'
                && tablero[0][2] == tablero[1][1]
                && tablero[0][2] == tablero[2][0]) {
            gano = 'd';
            return true;
        }

        return false;
    }

    public String getJugadorActual() {
        return turno ? j1 + "(" + MARCA_J1 + ")" : j2 + "(" + MARCA_J2 + ")";
    }

    public String getGanador() {
        return turno ? j2 + "(" + MARCA_J2 + ")" : j1 + "(" + MARCA_J1 + ")";
    }

    public void setJ1(String j1) {
        this.j1 = j1;
    }

    public void setJ2(String j2) {
        this.j2 = j2;
    }

    public boolean empate() {
        for (char[] fil : tablero) {
            for (char c : fil) {
                if (c == '_') {
                    return false;
                }
            }
        }
        return true;
    }

}
