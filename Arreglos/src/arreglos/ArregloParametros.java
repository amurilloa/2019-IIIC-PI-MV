/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class ArregloParametros {

    public int[] convertirPares(int[] arreglo) {
        int[] res = new int[arreglo.length];

        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                res[i] = arreglo[i] + 1;
            } else {
                res[i] = arreglo[i];
            }
        }

        return res;
    }
    
    public void convertirImpares(int[] arreglo){
        for (int i = 0; i < arreglo.length; i++) {
            if(arreglo[i]%2==0){
                arreglo[i]++;
            }
        }
    }

}
