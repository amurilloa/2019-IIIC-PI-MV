/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class Arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] numeros = {1, 2, 3, 4, 5, 6};
//        String[] palabras = new String[2];
//        palabras[0] = "Hola";
//        palabras[1] = "Mundo";
//        System.out.println(palabras[0]);
//        System.out.println(palabras[1]);

//        for (String palabra : palabras) {
//            System.out.println(palabra);
//        }
//
//        for (int i = 0; i < palabras.length; i++) {
//            System.out.println(palabras[i]);
//            palabras[i] = "Perro";
//        }
//        
//        for (String palabra : palabras) {
//            System.out.println(palabra);
//        }
//        int[] nums = new int[10];
//        //Pedir 10 números y meterlos en el arreglo
//        for (int i = 0; i < 10; i++) {
//            nums[i] = Util.leerInt("#" + (i + 1));
//        }
//
//        //Imprimir el arreglo final 
//        String txt = " - ";
//        for (int num : nums) {
//            txt += num + " - ";
//        }
//        Util.mostrar(txt);

        Perro[] perrera = new Perro[5];
        
        Perro p1 = new Perro(1, "Capitán", "Pastor");
        perrera[0]=p1;
        
        perrera[1] = new Perro(2, "Sussy", "Labrador");
        
        for (Perro perro : perrera) {
            System.out.println(perro);
        }

    }

}
