/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class MainParametros {

    public static void main(String[] args) {

        ArregloParametros log = new ArregloParametros();

        int[] numeros = {1, 3, 4, 7};

        for (int numero : numeros) {
            System.out.print(numero + " ");
        }
        System.out.println("");

        numeros = log.convertirPares(numeros); //Pasando un arreglo por parámetro, simulando paso de parámetro por valor

        for (int numero : numeros) {
            System.out.print(numero + " ");
        }
        System.out.println("");

        log.convertirImpares(numeros); //Paso de parámetro por referencia

        for (int numero : numeros) {
            System.out.print(numero + " ");
        }
        System.out.println("");

    }
}
