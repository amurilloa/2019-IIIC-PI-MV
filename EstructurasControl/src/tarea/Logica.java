/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    public String notas(int nota) {
        if (nota < 70) {
            return "Insuficiente";
        } else if (nota < 80) {
            return "Bien";
        } else if (nota < 90) {
            return "Notable";
        } else {
            return "Sobresaliente";
        }
    }

    public String musicos(int can, int par) {

        String tipo = "Músico en Formación";

        if (can >= 7 && can <= 10) {
            if (par == 0) {
                tipo = "Músico Naciente";
            } else if (par >= 1 && par <= 5) {
                tipo = "Músico Estelar";
            }
        } else if (can > 10 && par > 5) {
            tipo = "Músico Consagrado";
        }

        return "\u266B\u266B " + tipo + " \u266B\u266B";
    }

    public String cajero(int monto) {

        String detalle = "";

        int mon = 500;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 200;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 100;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 50;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 20;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 10;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 5;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de $%d\n", can, mon);
        }

        mon = 2;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d monedas de $%d\n", can, mon);
        }

        if (monto > 0) {
            detalle += "1 moneda de $1\n";
        }

        return detalle;
    }

    public double crucero(int can, int edad, int precio) {
        double desc = 0;
        if (can == 1) {
            if (edad >= 18 && edad <= 30) {
                desc = 0.078 * precio;
            } else if (edad > 30) {
                desc = 0.10 * precio;
            }
        } else if (can == 2) {
            desc = 0.115 * precio * can;
        } else if (can > 3) {
            desc = 0.15 * precio * can;
        }
        return desc;
    }

}
