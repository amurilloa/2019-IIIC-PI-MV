/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class InterfazDos {

    public static void main(String[] args) {
        Calculadora cal = new Calculadora();
        String[] opciones = {"(+) Sumar", "(-) Restar", "(*) Multiplicar", "(/) Dividir","(s) Salir"};

        APP:
        while (true) {
            String sel = (String) JOptionPane.showInputDialog(null, "Seleccione una opción", "Calculadora",
                    JOptionPane.QUESTION_MESSAGE, null, opciones, opciones[0]);
            char op = sel == null ? 's' : sel.charAt(1);
            switch (op) {
                case 's':
                    break APP;
                case '+':
                    int n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    int n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    int res = cal.sumar(n1, n2);
                    String txt = String.format("%d + %d = %d", n1, n2, res);
                    JOptionPane.showMessageDialog(null, txt);
                    break;
                case '-':
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    res = cal.restar(n1, n2);
                    txt = String.format("%d - %d = %d", n1, n2, res);
                    JOptionPane.showMessageDialog(null, txt);

                    break;
                case '*':
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    res = cal.multiplicar(n1, n2);
                    txt = String.format("%d * %d = %d", n1, n2, res);
                    JOptionPane.showMessageDialog(null, txt);

                    break;
                case '/':
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    double div = cal.dividir(n1, n2);
                    txt = String.format("%d / %d = %.2f", n1, n2, div);
                    JOptionPane.showMessageDialog(null, txt);
                    break;
            }
        }
    }
}
