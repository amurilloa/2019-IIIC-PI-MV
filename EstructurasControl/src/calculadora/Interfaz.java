/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Interfaz {

    public static void main(String[] args) {
        Calculadora cal = new Calculadora();
        String menu = "Calculadora - UTN v0.1\n"
                + "(+) Sumar\n"
                + "(-) Restar\n"
                + "(*) Multiplicar\n"
                + "(/) Dividir\n"
                + "(s) Salir\n"
                + "Seleccione una opción: ";

        while (true) {
            char op = JOptionPane.showInputDialog(menu).charAt(0);
            if (op == 's' || op == 'S') {
                break;
            } else {
                int n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                int n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                double res = cal.resolver(n1, n2, op);
                
                String form = op == '/' ? "%.2f" : "%.0f"; //if de asignación                
                String txt = String.format("%d %s %d = " + form, n1, op, n2, res);
                JOptionPane.showMessageDialog(null, txt);
            }
        }
    }
}
