/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    public String ejercicioUno(int num) {
        String txt = "";

        if (num < 0) {
            txt = "Es negativo";
        } else {
            txt = "Es positivo";
        }

        if (num % 5 == 0) {
            txt += " y es divisible entre 5";
        } else {
            txt += " y no es divisible entre 5";
        }
        return txt;
    }

    public int ejercicioDos(int min, int max) {
        int ale = (int) (Math.random() * max) + min;
        return ale;
    }

    public double ejercicioTres(int pas, int ejes, double costo) {
        double impBase = costo * 0.01;
        double impPas = 0;
        double impEjes = 0;

        if (pas < 20) {
            impPas = impBase * 0.01;
        } else if (pas >= 20 && pas <= 60) {
            impPas = impBase * 0.05;
        } else {
            impPas = impBase * 0.08;
        }

        if (ejes == 2) {
            impEjes = impBase * 0.05;
        } else if (ejes == 3) {
            impEjes = impBase * 0.10;
        } else if (ejes > 3) {
            impEjes = impBase * 0.15;
        }

        double costoTotal = costo + impBase + impPas + impEjes;
        return costoTotal;
    }

    public String obtenerDía(int numDia) {
        String dia = "";

        switch (numDia) {
            case 1:
                dia = "Domingo";
                break;
            case 2:
                dia = "Lunes";
                break;
            case 3:
                dia = "Martes";
                break;
            case 4:
                dia = "Miércoles";
                break;
            case 5:
                dia = "Jueves";
                break;
            case 6:
                dia = "Viernes";
                break;
            case 7:
                dia = "Sábado";
                break;
            default:
                dia = "Día inválido!!";
        }

        return dia;
    }

    public String obtenerDíaDos(int numDia) {

        switch (numDia) {
            case 1:
                return "Domingo";
            case 2:
                return "Lunes";
            case 3:
                return "Martes";
            case 4:
                return "Miércoles";
            case 5:
                return "Jueves";
            case 6:
                return "Viernes";
            case 7:
                return "Sábado";
            default:
                return "Día inválido!!";
        }
    }
}
