/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Ciclos {

    public static void main(String[] args) {
        int con = 1;
        while (con <= 10) {
            System.out.print(con + " ");
            con++;
        }

        con = 10;
        while (con >= 0) {
            System.out.print(con + " ");
            con--;
        }

        String menu = "1. Ejercicio #1\n"
                + "2. Ejercicio #2\n"
                + "3. Salir\n"
                + "Seleccione una opción: ";

        //boolean salir = false;
        MENU:
        while (true) { //!salir
            int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
            switch (op) {
                case 1:
                    System.out.println("Ejercicio 1");
                    break;
                case 2:
                    System.out.println("Ejercicio 2");
                    System.out.println("Ejercicio 2");
                    break;
                case 3:
                    System.out.println("Gracias por usuar mi app!!!");
                    //salir = true;
                    break MENU;
                default:
                    System.out.println("Opción inválida!!");
            }
        }
    }
}
