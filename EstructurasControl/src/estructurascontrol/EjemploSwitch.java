/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class EjemploSwitch {

    public static void main(String[] args) {
        
        Logica log = new Logica();
        System.out.println(log.obtenerDía(12));
        
//        String menu = "1. Ejercicio #1\n"
//                + "2. Ejercicio #2\n"
//                + "3. Salir\n"
//                + "Seleccione una opción: ";
//        int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
//        switch (op) {
//            case 1:
//                System.out.println("Ejercicio 1");
//                break;
//            case 2:
//                System.out.println("Ejercicio 2");
//                System.out.println("Ejercicio 2");
//                break;
//            case 3:
//                System.out.println("Gracias por usuar mi app!!!");
//                break;
//            default:
//                System.out.println("Error!!");
//        }
//        
    }
    
}

//public String dia(int num){
//    1 --> Domingo
//    2 --> Lunes 
//    ...e
//    7 --> Sábado
//    8 --> Día inválido
//}
