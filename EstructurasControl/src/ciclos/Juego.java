/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

/**
 *
 * @author Allan Murillo
 */
public class Juego {

    private final int aleatorio;
    private int intento;

    public final int NUM_MAX = 10;
    public final int MAX_INT = 5;

    public Juego() {
        intento = 1;
        aleatorio = (int) (Math.random() * NUM_MAX) + 1;
    }

    public String adivinar(int num) {
        String txt = "";
        if (num == aleatorio) {
            txt = "Ganaste adivinaste el número aleatorio (intento #" + intento + ")";
            intento = MAX_INT + 1;
        } else if (intento == MAX_INT) {
            txt = "Perdiste se acabaron sus intentos";
        } else if (num < aleatorio) {
            txt = "Digite un número mayor";
        } else {
            txt = "Digite un número menor";
        }
        intento++;
        return txt;
    }

    public boolean termino() {
        return intento > MAX_INT;
    }

    public int getIntento() {
        return intento;
    }

}
