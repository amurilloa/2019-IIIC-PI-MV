/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import pintor.Util;

/**
 *
 * @author Allan Murillo
 */
public class Principal {

    public static void main(String[] args) {
        Juego j = new Juego();
        while (!j.termino()) {
            int num = Util.leerInt("Digite un número entre 1 - " + j.NUM_MAX);
            Util.mostrar(j.adivinar(num));
        }

    }

}
