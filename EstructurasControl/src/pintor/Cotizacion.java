/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pintor;

/**
 *
 * @author Allan Murillo
 */
public class Cotizacion {

    private String cliente;
    private double totalP;
    private double totalV;

    private final int COSTO_X_HORA = 30;
    private final int MIN_X_METRO = 10;
    private final int HRS_X_DIA = 10;

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public void agregarPared(double al, double an) {
        Rectangulo r = new Rectangulo(al, an);
        totalP += r.getArea();
    }

    public double getTotalP() {
        return totalP;
    }

    public void agregarVentana(double al, double an) {
        Rectangulo r = new Rectangulo(al, an);
        totalV += r.getArea();
    }

    public void agregarVentana(double dia) {
        Circunferencia c = new Circunferencia(dia / 2);
        totalV += c.getArea();
    }

    private String calcularTiempo(double pintar) {
        int min = (int) Math.round(pintar * MIN_X_METRO + 0.5);
        int hrs = min / 60;
        int dias = hrs / HRS_X_DIA;
        hrs = hrs % HRS_X_DIA;
        min = min % 60;
        return String.format("%d días, %d hrs, %d min", dias, hrs, min);
    }

    private double calcularCosto(double pintar) {
        return (pintar * MIN_X_METRO) / 60 * COSTO_X_HORA;
    }

    @Override
    public String toString() {
        double pintar = totalP - totalV;
        String txt = "Cliente: %s\n"
                + "Área Paredes: %.2fm²\n"
                + "Área Ventanas: %.2fm²\n"
                + "Área a Pintar: %.2fm²\n"
                + "Tiempo Aprox.: %s\n"
                + "Costo Total: $%.2f";
        String tiempo = calcularTiempo(pintar);
        double costo = calcularCosto(pintar);

        return String.format(txt, cliente, totalP, totalV, pintar, tiempo, costo);
    }
}
