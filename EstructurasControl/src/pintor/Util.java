/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pintor;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Util {

    private static final String TITULO = "Pintor UTN - v0.1";

    public static void mostrar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, TITULO, JOptionPane.INFORMATION_MESSAGE);
    }

    public static int leerInt(String mensaje) {
        String error = "";
        while (true) {
            try {
                int op = Integer.parseInt(JOptionPane.showInputDialog(
                        null, error + mensaje, TITULO, JOptionPane.QUESTION_MESSAGE));
                return op;
            } catch (NumberFormatException e) {
                error = "Formato Inválido!\n";
            }
        }
    }

    public static double leerNum(String mensaje) {
        String error = "";
        while (true) {
            try {
                double op = Double.parseDouble(JOptionPane.showInputDialog(
                        null, error + mensaje, TITULO, JOptionPane.QUESTION_MESSAGE));
                return op;
            } catch (NumberFormatException e) {
                error = "Formato Inválido!\n";
            }
        }
    }

    public static boolean confirmar(String mensaje) {
        int res = JOptionPane.showConfirmDialog(null, mensaje, TITULO,
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        return res == JOptionPane.YES_OPTION;
    }

    public static String seleccionar(String mensaje, String[] opciones) {
        String sel = (String) JOptionPane.showInputDialog(null, mensaje, TITULO,
                JOptionPane.QUESTION_MESSAGE, null, opciones, opciones[0]);
        return sel;
    }

    public static String leerTexto(String mensaje) {
        String txt = JOptionPane.showInputDialog(null, mensaje, TITULO,
                JOptionPane.QUESTION_MESSAGE);
        return txt;
    }
}
