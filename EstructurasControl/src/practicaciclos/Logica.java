/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaciclos;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    //Variable Global - Atributo de Clase
    private int total;

    public String generarTabla(int tabla) {
        String res = "";
        int cont = 0;
        while (cont < 10) {
            res += String.format("%d x %d = %d\n", tabla, cont, (tabla * cont));
            cont++;
        }
        return res;
    }

    public String multiplos(int num, int max) {
        String res = "";
        String car = " ";
        for (int i = 1; i <= max; i++) {
            car = i % 20 == 0 ? "\n" : " ";
            if (i % num == 0) {
                res += i + car;
            }
        }
        return res;
    }

    public void sumatoria(int num) {
        total += num;
    }

    public int getTotal() {
        return total;
    }

}
