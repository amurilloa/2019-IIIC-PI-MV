/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaciclos;


import pintor.Util;
/**
 *
 * @author Allan Murillo
 */
public class PrincipalFor {

    public static void main(String[] args) {
//        String txt = "Hola Mundo";
//
//        for (int i = 0; i < txt.length(); i++) {
//            System.out.println(txt.charAt(i));
//        }
//
//        String[] opciones = {"Uno", "Dos", "Tres"};
//        
//        for (String x : opciones) {
//            System.out.println(x);
//        }

        Logica log = new Logica();
        Util.mostrar(log.multiplos(7, 1000));

    }
}
